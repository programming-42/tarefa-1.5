# Readme

## Tarefa 1.5 de programação 4

### Descrição

Esta atividade se trata da primeira tarefa de progrmação do módulo 4.

Nesta atividade foi preciso desenvolver uma API para o cadastro de jogos. Foi necessário fazer um CRUD básico para cumprir esse desafio.

### Rotas:

#### Get:
Listar todos os jogos: http://localhost:5000/game

Listar jogo por id: http://localhost:5000/game/{id}

#### post:
Rota: http://localhost:5000/game

```json 
{
    "name": "GTA",
    "description": "GTA é um jogo de mundo aberto",
    "genre": "Violência",
    "plataform": "pc"
}
```

#### Put:
Rota: http://localhost:5000/game/{id}
```json 
{
    "name": "GTA3",
    "description": "GTA é um jogo de mundo fechado",
    "genre": "Ação",
    "plataform": "Celular"
}
```

#### Patch:

```json 
{
    "name": "GTA4",
}
```

#### Delete:

Deletar jogo pelo Id: http://localhost:5000/game/{id}

### Imagens de funcionamento:
Adicionando um Jogo:

![Execução do post](./prints/PostNewGame.png)


Listando todos os jogos:
![Execução do get](./prints/GetAllGames.png)

Mostrando jogo por ID:
![Execução do get](./prints/GetById.png)

Atualizando todos os dados do jogo:

![Execução do put](./prints/GameUpdate.png)
Mostrando que os dados foram atualizados:
![Execução do get](./prints/ShowGameUpdated.png)

Atualizando parte de um jogo:
![Execução do patch](./prints/UpdatePartial.png)
Mostrando que o nome foi atualizado:
![Execução do get](./prints/ShowPartialUpdate.png)

Deletando Jogo:
![Execução do delete](./prints/Deleted.png)
Mostrando que o jogo foi deletado:
![Execução do get](./prints/ShowDeleted.png)
