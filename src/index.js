import express from "express"
import errorMiddleware from "./middleware/errorMiddleware.js"
import gameController from "./controller/gameController.js"
import dotenv from "dotenv"


const app = express()

app.use(express.json())

app.use("/game", gameController)

app.use(errorMiddleware, )

dotenv.config()

app.listen(process.env.PORT, () => {
    console.log(`port ${process.env.PORT}`)
})