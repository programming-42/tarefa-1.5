import { DataTypes } from "sequelize";
import sequelize from "../database.js";

const Game = sequelize.define(
    "Game",

    {
        id: {
            type: DataTypes.INTEGER,
            autoIncrement: true,
            primaryKey: true,
        },

        name: {
            type: DataTypes.TEXT
        },

        description: {
            type: DataTypes.TEXT
        },
        genre: {
            type: DataTypes.TEXT
        },
        plataform: {
            type: DataTypes.TEXT
        },
    },

    {
        sequelize,
        tableName: "game",
        paranoid: false,
        createdAt: false,
        updatedAt: false,
        deletedAt: false,
    }
)

export default Game