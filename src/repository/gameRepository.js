import Game from "./models/gameModel.js";

async function listGame(){
    return await Game.findAll()
}

async function getGameById(id) {
    return await Game.findByPk(id)
}

async function createGame(name, description, genre, plataform){
    await Game.create({name, description, genre, plataform})
}

async function updateGame(id, game){
    return await Game.update(game, { where: { id } })
}

async function deleteGameById(id){
    const game = await Game.findByPk(id)
    game.destroy()
}

async function updateGamePartial(id, updates){
    return await Game.update(updates, { where: { id } })
}

export {listGame, getGameById, createGame, updateGame, deleteGameById, updateGamePartial}