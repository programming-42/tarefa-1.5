import express from "express"
import {listGame, getGameById, createGame, updateGame, deleteGameById, updateGamePartial} from "../repository/gameRepository.js"

const gameController = express.Router()

gameController.get("/", async (req, res) => {
    const games = await listGame()

    return res.send(games)
})

gameController.get("/:id", async (req, res) => {
    const game = await getGameById(req.params.id)

    return res.send(game)
})

gameController.post("/", (req, res) => {
    const {name, description, genre, plataform} = req.body
    createGame(name, description, genre, plataform)

    return res.status(201).send("user created")
})

gameController.put("/:id", (req, res) => {
    const id = req.params.id
    const game = req.body

    updateGame(id, game)

    return res.send("user updated")
})

gameController.patch("/:id", (req, res) => {
    const partToUpdate = req.body

    updateGamePartial(req.params.id, partToUpdate)

    return res.send("user updated")
})

gameController.delete("/:id", (req, res) => {
    deleteGameById(req.params.id)
    return res.status(204).send("user deleted")
})

export default gameController